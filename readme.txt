The rpmsg_proto_socket_test application sends and receives packets through
the rpmsg_proto socket interface.

The application sends first a short init_msg followed by a full size message.

Then the applicaiton waits for a recieve message. Once received the content is
 to checked to see if the looped back message matches with sent packet and
 prints success or failure accordingly.
(Note any looped back init(short) message is ignored.)

To compile
----------

Set cross compile as follows
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
PATH=$HOME/gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf/bin:$PATH

Then just execute make at the root directory

The executable binary "rpmsg_proto_socket_test" will be created at the root directory itself.

To run
------

Copy the executable "rpmsg_proto_socket_test" to the filsystem and execute.
